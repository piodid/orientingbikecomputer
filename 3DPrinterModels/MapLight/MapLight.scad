include<MapLightVars.scad>

screwM6Diameter=7;
mapHolderLength=100;
mapHolderLightHeight=50;

difference()
{
	union()
	{
		translate([-totalHolderWidth/2,-holderLength/2,0])
		cube([totalHolderWidth,holderLength,holderHeight]);
		
		translate([totalHolderWidth/2,-connectorWidth/2,0])
		cube([mapHolderLength,connectorWidth,connectorHeight]);
		
		translate([totalHolderWidth/2+mapHolderLength,-connectorWidth/2,0])
		cube([connectorHeight,connectorWidth,mapHolderLightHeight]);
		
		
		translate([totalHolderWidth/2+mapHolderLength,-latchScrewDistance/2,mapHolderLightHeight-latchHeight/2])
		cube([connectorHeight,latchScrewDistance,latchHeight]);
	}

	translate([-mapHolderConnectorWidth/2,-holderLength/2-1,holderHeight-3])
	cube([mapHolderConnectorWidth,holderLength+2,3+1]);

	translate([0,0,-1])
	cylinder(d=screwM6Diameter,h=holderHeight+2,$fn=16);
}