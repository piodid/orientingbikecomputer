include<MapLightVars.scad>

collimatorHolderLatch();

module collimatorHolderLatch()
{
	union()
	{
		collimatorHolderLatchHalf();
		
		mirror([0,1,0])
		collimatorHolderLatchHalf();
	}
}

module collimatorHolderLatchHalf()
{
	cos30=sqrt(3)/2;

	d=holderHeight;
	r=d/2;
	x=latchScrewDistance+holderHeight;
	y=latchScrewDistance;
	
	difference()
	{
		union()
		{
			intersection()
			{
				translate([r/2,-r*cos30,r])
				rotate([0,90,30])
				cylinder(d=d,h=x,$fn=32);
				
				linear_extrude(d)
				{
					polygon([
						[0,0],
						[x*cos30,x/2],
						[x*cos30,0],
					]);
				}
			}

			translate([d*cos30,0,0])
			linear_extrude(d)
			{
				polygon([
					[0,0],
					[y*cos30,y/2],
					[y*cos30,0],
				]);
			}
		}
		
		translate([0,y/2,r])
		rotate([0,90,0])
		cylinder(d=5,h=x*cos30-6,$fn=6);
		
		translate([0,y/2,r])
		rotate([0,90,0])
		cylinder(d=3,h=x*cos30+2,$fn=6);
	}
}

/*
translate([d,0,0])
rotate([0,90,30])
cylinder(d=d,h=100,$fn=32);
*/

/*
//translate([-wiresHoleWidth/2,0,0])
//rotate([0,90,0])
linear_extrude(wiresHoleWidth)
{
	polygon([
		[0,0],
		[x*sqrt(3),x/2],
		[x*sqrt(3),-x/2],
		//[-wiresHole_Pt2_H,0],
		//[-wiresHole_Pt3_H,0],
		//[-wiresHole_Pt3_H,wiresHole_Pt3_Y],
		//[-wiresHole_Pt4_H,wiresHole_Pt4_Y]
	]);
}
*/