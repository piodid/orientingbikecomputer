holderWidth=10;
holderLength=30;
holderHeight=8;
connectorWidth=8;
connectorHeight=8;

mapHolderConnectorWidth=8;

totalHolderWidth=mapHolderConnectorWidth+2*holderWidth;

collimatorDiameter=20;
collimatorRadius=collimatorDiameter/2;
collimatorHolderThickness=2;
collimatorHolderTotalDiameter=collimatorDiameter+2*collimatorHolderThickness;
collimatorHolderTotalRadius=collimatorHolderTotalDiameter/2;

collimatorHolderHeight=16;

collimatorHolderBaseThickness=2;
collimatorHolderInnerDiameter=8;
collimatorHolderInnerHeight=4;

angle=15;

screwM25Diameter=3;
screwM25HeadDiameter=6;
screwM25HeadHeight=2;
//screwM25HolderDiameter=8;
//screwM25HolderRadius=screwM3HolderDiameter/2;

nutM25Height=2.4;
nutM25ConeHeight=1.0;
nutM25Diameter=7;

latchHeight=4;
latchScrewDistance=collimatorHolderTotalDiameter+holderHeight;

wiresHoleWidth=4;

//TODO 
// Check variables
// Latch on main holder
// Holder cover
