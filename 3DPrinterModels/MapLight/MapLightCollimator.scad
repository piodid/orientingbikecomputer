include<MapLightVars.scad>

sinAng=sin(angle);
cosAng=cos(angle);

letchSinAng=latchHeight*sinAng;
letchCosAng=latchHeight*cosAng;
holderSinAng=holderHeight*sinAng;
holderCosAng=holderHeight*cosAng;

hh=holderCosAng+letchSinAng;
aa=-holderSinAng+letchCosAng;

union()
{
	
	difference()
	{
		union()
		{
			difference()
			{
				cylinder(d=collimatorHolderTotalDiameter,h=collimatorHolderHeight,$fn=64);
				
				translate([-collimatorHolderTotalRadius-1,-collimatorHolderTotalRadius,-1])
				cube([collimatorHolderTotalDiameter+2,aa,holderHeight/2+1]);
			}
			
			translate([-collimatorHolderTotalRadius,0,0])
			rotate([0,90,0])
			linear_extrude(collimatorHolderTotalDiameter)
			{
				polygon([
					[0,0],
					[-holderCosAng,0],
					[-holderCosAng,-collimatorHolderTotalRadius],
					[0,-collimatorHolderTotalRadius+aa]
				]);
			}
			
			latch();
		}
			
		translate([0,0,collimatorHolderBaseThickness])
		cylinder(d=collimatorDiameter,h=collimatorHolderHeight+2,$fn=64);
		
		wiresHole();
	}
	
	translate([0,0,collimatorHolderBaseThickness])
	cylinder(d=collimatorHolderInnerDiameter,h=collimatorHolderInnerHeight,$fn=32);
}

module latch()
{
	translate([0,-collimatorHolderTotalDiameter/2,hh])
	rotate([-90-angle,0,0])
	difference()
	{
		linear_extrude(latchHeight)
		{
			translate([-latchScrewDistance/2,0])
			square([latchScrewDistance,holderHeight]);
			
			for(i=[-1,1])
			translate([i*latchScrewDistance/2,holderHeight/2])
			circle(d=holderHeight,$fn=32);
		}
		
		for(i=[-1,1])
		{
			translate([i*latchScrewDistance/2,holderHeight/2,0])
			{
				translate([0,0,latchHeight-screwM25HeadHeight])
			cylinder(d1=screwM25Diameter,d2=screwM25HeadDiameter,h=screwM25HeadHeight+0.01,$fn=16);
			
			translate([0,0,-1])
			cylinder(d=screwM25Diameter,h=latchHeight+2,$fn=16);
			}
		}
	}
}

module wiresHole()
{
	wiresHole_TopHole=(holderHeight-wiresHoleWidth)/2;
	wiresHole_BottomHole=(holderHeight+wiresHoleWidth)/2;

	wiresHole_Pt1_H=hh-wiresHole_TopHole*cosAng;
	wiresHole_Pt1_Y=-collimatorHolderTotalRadius-wiresHole_TopHole*sinAng-0.01;
	wiresHole_Pt2_H=hh-wiresHole_TopHole*cosAng-letchSinAng;
	wiresHole_Pt2_Y=-collimatorHolderTotalRadius-wiresHole_TopHole*sinAng+letchCosAng+0.01;

	wiresHole_Pt4_H=hh-wiresHole_BottomHole*cosAng;
	wiresHole_Pt4_Y=-collimatorHolderTotalRadius-wiresHole_BottomHole*sinAng-0.01;
	wiresHole_Pt3_H=max(hh-wiresHole_BottomHole*cosAng-letchSinAng,collimatorHolderBaseThickness);
	//wiresHole_Pt3_Y-collimatorHolderTotalRadius- wiresHole_BottomHole*sinAng+letchCosAng+0.01;
	wiresHole_Pt3_Y=-sqrt(collimatorRadius*collimatorRadius-wiresHoleWidth*wiresHoleWidth/4)+0.01;
	
	translate([-wiresHoleWidth/2,0,0])
	rotate([0,90,0])
	linear_extrude(wiresHoleWidth)
	{
		polygon([
			[-wiresHole_Pt1_H,wiresHole_Pt1_Y],
			[-wiresHole_Pt2_H,wiresHole_Pt2_Y],
			//[-wiresHole_Pt2_H,0],
			//[-wiresHole_Pt3_H,0],
			[-wiresHole_Pt3_H,wiresHole_Pt3_Y],
			[-wiresHole_Pt4_H,wiresHole_Pt4_Y]
		]);
	}
}