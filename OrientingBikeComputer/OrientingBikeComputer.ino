#include <SoftwareSerial.h>
#include "DisplayController.h"
#include "ButtonsController.h"
#include "Pins.h"

#define DEBUG DEBUG

#define BLUETOOTH_ADDRESS "E79CD66AE43E"
#define BLUETOOTH_BUFF_SIZE 32
#define MAX_CONNECTION_TRIES 10
#define DEFAULT_WHEEL_SIZE 2300

// STATE
#define STATE_STANDBY 0
#define STATE_RUNNING 1
#define STATE_SET_BLUETOOTH_ADDRESS 2
#define STATE_SETTING_BLUETOOTH_ADDRESS 3
#define STATE_SET_WHEEL_SIZE 4
#define STATE_SETTING_WHEEL_SIZE 5

// BLUETOOTH (HM-10): RX, TX
SoftwareSerial bluetoothSerial(PIN_BLUETOOTH_RX, PIN_BLUETOOTH_TX); 
DisplayController displayController;


byte state = STATE_STANDBY;

bool error = false;
bool errorDislayed = false;
byte failConnectionTryCount = 0;
byte buff[BLUETOOTH_BUFF_SIZE];
byte buff_index = 0;

bool neverConnected = true;

bool increase = true;

unsigned long wheelSize = DEFAULT_WHEEL_SIZE;


unsigned long distance_one = 0;
unsigned long distance_two = 0;
bool display_paused_one = false;
bool display_paused_two = false;


bool last_wheel_revolutions_assigned = false;
unsigned long last_wheel_revolutions = 0;

byte display_long_string_last_character = 0;
byte display_long_string_state;



void setup() 
{
	/*
	pinMode(PIN_BLUETOOTH_STATE, INPUT);
	pinMode(PIN_BLUETOOTH_BREAK, OUTPUT);

	digitalWrite(PIN_BLUETOOTH_BREAK, LOW);
	while(true)
	{
		int state = digitalRead(PIN_BLUETOOTH_STATE);
		if (state == LOW) {
			break;
		}
		delay(200);
	}

	digitalWrite(PIN_BLUETOOTH_BREAK, HIGH);
	delay(500);
	*/

	displayController.init();
	displayController.displayLongString("I LOVE YOU SO MUCH", 18);
	//displayController.displayString("12345678", 8);
	//displayController.displayString("87654321", 8);
	//delay(2000);
	//displayController.displayString("12345678", 8);
	

	/*
	#ifdef DEBUG
	Serial.begin(9600);
	#endif
	bluetoothSerial.begin(9600);

	
	sendCommand("AT+IMME1");
	sendCommand("AT+ROLE1");
	sendCommand("AT+UUID0x1816");
	sendCommand("AT+CHAR0x2A5B");
	sendCommand("AT+COMP1");
	*/

	/*
	sendCommand("AT+ROLE1");
	sendCommand("AT+IMME1");
	sendCommand("AT+UUID0x1816");
	sendCommand("AT+CHAR0x2A5B");
	sendCommand("AT+NOTI0");
	sendCommand("AT+TYPE0");
	sendCommand("AT+SHOW1");
	sendCommand("AT+COMP1");
	sendCommand("AT+MODE2");
	*/
	/*
	sendCommand("AT+ROLE?");
	sendCommand("AT+IMME?");
	sendCommand("AT+NOTI?");
	sendCommand("AT+SHOW?");
	sendCommand("AT+TYPE?");
	sendCommand("AT+UUID?");
	sendCommand("AT+CHAR?");
	sendCommand("AT+COMP?");
	sendCommand("AT+MODE?");
	*/
}


void loop() 
{
	displayController.updateLongString();
	delay(50);
	return;

	if (error)
	{
		if (!errorDislayed)
		{
			displayController.displayError();
			errorDislayed = true;
		}
		delay(1000);
		return;
	}



	if (state == STATE_RUNNING)
	{
		checkSensorConnection();

		while (bluetoothSerial.available() && buff_index < BLUETOOTH_BUFF_SIZE)
		{
			byte b = bluetoothSerial.read();
			buff[buff_index++] = b;
		}

		checkBluetoothBuffer();
		displayController.updateDistance();

		delay(100);
		return;
	}

  
}

void checkSensorConnection()
{
  int bluetoothSensorState = digitalRead(PIN_BLUETOOTH_STATE);
  if (bluetoothSensorState == LOW)
  {
    if (!connectToSensor()) 
    {
      failConnectionTryCount++;

      if (failConnectionTryCount == MAX_CONNECTION_TRIES)
      {
        error = true;
        #ifdef DEBUG
          Serial.print("Couln't connect to sensor!");
        #endif
      }
      
      return;
    }

    failConnectionTryCount = 0;
  }
}

void checkBluetoothBuffer()
{
  if (buff_index < 7)
  {
    return;
  }

  int startIndex = 0;
  while (startIndex + 14 <= buff_index)
  {
     startIndex += 7;
  }

  if (buff[startIndex] != 1)
  {
    #ifdef DEBUG
      Serial.print("Sensor is not set to measure speed!");
    #endif
    
    error = true;
    return;
  }

  unsigned long currentWheelRevolutions = buff[startIndex+1];
  for(int i=2; i<5; i++)
  {
    currentWheelRevolutions <<= 8;
    currentWheelRevolutions |= buff[startIndex+i];
  }

  if (!last_wheel_revolutions_assigned)
  {
    last_wheel_revolutions = currentWheelRevolutions;
    last_wheel_revolutions_assigned = true;
  }
  else
  {
    if (currentWheelRevolutions != last_wheel_revolutions)
    {
      unsigned long diffWheelRevolutions = currentWheelRevolutions - last_wheel_revolutions;
      
      if (!display_paused_one)
      {
        distance_one += diffWheelRevolutions * wheelSize;
      }
      
      if (!display_paused_two)
      {
        distance_two += diffWheelRevolutions * wheelSize;
      }
      
      last_wheel_revolutions = currentWheelRevolutions;
    }
  }

  shiftBuff(startIndex+7);
}

void shiftBuff(int count)
{
  buff_index -= count;
  for (int i=0; i<buff_index; i++)
  {
    buff[i] = buff[i + count];
  }
}

void sendCommand(const char * command) 
{
  #ifdef DEBUG
    Serial.print("Command send: ");
    Serial.println(command);
  #endif
  
  bluetoothSerial.println(command);
  int bytes_read = bluetoothSerial.readBytes(buff, BLUETOOTH_BUFF_SIZE);
  if (bytes_read > 0)
  {
    Serial.write(buff, bytes_read);
    Serial.println();
  }
  
  Serial.println("Reply end");
}

bool connectToSensor() {
  #ifdef DEBUG
    Serial.println("Connecting to " BLUETOOTH_ADDRESS);
  #endif

  bluetoothSerial.println("AT+CO1" BLUETOOTH_ADDRESS);
  int bytes_read = bluetoothSerial.readBytes(buff, 8);
  //Serial.println(bytes_read);
  //Serial.write(buff, bytes_read);
  if (bytes_read != 8 || strncmp(buff, "OK+CO11A", 8) != 0) 
  {
    #ifdef DEBUG
      Serial.println("No response after connection request");
    #endif
    error = true;
    return false;
  }
  
  bluetoothSerial.setTimeout(15000);
  bytes_read = bluetoothSerial.readBytes(buff, 8);
  bluetoothSerial.setTimeout(1000);
  //Serial.println(bytes_read);
  //Serial.write(buff, bytes_read);
  if (bytes_read < 8) 
  {
    #ifdef DEBUG
      Serial.println("No response after connection request");
    #endif
    error = true;
    return false;
  }

  if (strncmp(buff, "OK+CONNF", 8) == 0)
  {
    // No error, just not connected
    return false;
  }

  buff_index = 8;
  return true;
}
