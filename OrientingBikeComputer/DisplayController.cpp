#include "DisplayController.h"
#include "Pins.h"

DisplayController::DisplayController()
	: ledControl(PIN_DISPLAY_DATA_IN, PIN_DISPLAY_CLK, PIN_DISPLAY_CS)
{
}

// DisplayController::~DisplayController() { }

void DisplayController::init()
{
	ledControl.shutdown(0, false);
	ledControl.setIntensity(0, 7);
	ledControl.clearDisplay(0);
}

void DisplayController::updateDistance()
{
	unsigned long currentTime = millis();
	if (currentTime - lastDisplayUpdateTime > DISPLAY_REFRESH_TIME)
	{
		ledControl.clearDisplay(0);
		updateOneDisplay(*ptr_distance_one, 0);
		updateOneDisplay(*ptr_distance_one, 4);
	}
}

void DisplayController::updateOneDisplay(unsigned long distance, byte digit)
{
	distance /= 10000;

	for (int i = 0; i < 4; i++)
	{
		byte digit = distance % 10;
		distance /= 10;
		ledControl.setDigit(0, i + digit, digit, i == 2);
	}
}

#pragma region Display string

void DisplayController::displayLongString(const char * string, byte stringLength)
{
	#ifdef DEBUG
	if (strlen(string) <= 8) {
		Serial.write("String is not long!");
	}
	#endif

	characterDisplayed = 8;
	longString = string;
	longStringLength = stringLength;
	lastDisplayUpdateTime = millis();

	displayString(string, 8);
}

void DisplayController::updateLongString()
{
	unsigned long currentTime = millis();
	if (currentTime - lastDisplayUpdateTime < DISPLAY_LONG_STRING_SHIFT_TIME) {
		return;
	}

	lastDisplayUpdateTime = currentTime;

	if (characterDisplayed >= longStringLength + SPACES_AFTER_LONG_STRING) {
		characterDisplayed = 1;
	} else {
		characterDisplayed++;
	}

	byte stringTailLength = 0, stringTailOffset = 0;
	byte stringLength = 0, stringOffset = 0;
	byte spacesBefore = 0, spacesAfter = 0;

	if (characterDisplayed < 8)
	{
		stringLength = characterDisplayed;
		if (characterDisplayed + SPACES_AFTER_LONG_STRING < 8)
		{
			stringTailLength = 8 - characterDisplayed - SPACES_AFTER_LONG_STRING;
			stringTailOffset = longStringLength - stringTailLength;
			spacesBefore = SPACES_AFTER_LONG_STRING;
		}
		else
		{
			spacesBefore = 8 - characterDisplayed;
		}
	}
	else
	{
		stringOffset = characterDisplayed - 8;
		if (characterDisplayed > longStringLength) {
			stringLength = 8 + longStringLength - characterDisplayed;
			spacesAfter = 8 - stringLength;
		}
		else {
			stringLength = 8;
		}
	}

	char *buffPtr = buff;
	if (stringTailLength > 0) {
		strncpy(buffPtr, longString + stringTailOffset, stringTailLength);
		buffPtr += stringTailLength;
	}

	if (spacesBefore) {
		memset(buffPtr, ' ', spacesBefore);
		buffPtr += spacesBefore;
	}

	strncpy(buffPtr, longString + stringOffset, stringLength);
	buffPtr += stringLength;

	if (spacesAfter) {
		memset(buffPtr, ' ', spacesAfter);
	}

	displayString(buff, 8);
}

void DisplayController::displayString(const char * string, byte stringLength)
{
	if (stringLength < 8) {
		ledControl.clearDisplay(0);
	}

	for (char i=0, j=stringLength-1; j>=0; i++, j--)
	{
		
		switch(string[j])
		{
			case 'C':
				ledControl.setRow(0, i, B01001110);
				break;
			case 'M':
			case 'N':
				ledControl.setRow(0, i, B01110110);
				break;
			case 'S':
				ledControl.setChar(0, i, '5', false);
				break;
			case 'V':
			case 'U':
				ledControl.setRow(0, i, B00111110);
				break;
			case 'v':
			case 'u':
				ledControl.setRow(0, i, B00011100);
				break;
			case 'I':
				ledControl.setRow(0, i, B00000110);
				break;
			case 'Y':
				ledControl.setChar(0, i, '4', false);
				break;
			case 'O':
				ledControl.setChar(0, i, '0', false);
				break;
			default: 
				ledControl.setChar(0, i, string[j], false);
		}
	}
}

#pragma endregion Display string

void DisplayController::displayError()
{
	ledControl.clearDisplay(0);
	ledControl.setChar(0, 0, 'e', false);
	ledControl.setRow(0, 6, 0x05);
	ledControl.setRow(0, 5, 0x05);
}
