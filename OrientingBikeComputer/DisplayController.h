#ifndef DISPLAY_CONTROLLER_H
#define DISPLAY_CONTROLLER_H

#include <LedControl.h>

#define DEFAULT_DISPLAY_INTENSITY 7
#define DISPLAY_REFRESH_TIME 2000 // ms
#define DISPLAY_LONG_STRING_SHIFT_TIME 500 // ms
#define SPACES_AFTER_LONG_STRING 3

class DisplayController
{
public:
	DisplayController();
	//~DisplayController();
	void init();
	void updateDistance();
	void displayError();

	void displayLongString(const char * string, byte stringLength);
	void updateLongString();
	void displayString(const char * string, byte stringLength);

private:
	LedControl ledControl;
	unsigned long *ptr_distance_one = nullptr;
	unsigned long *ptr_distance_two = nullptr;
	byte intensity = DEFAULT_DISPLAY_INTENSITY;
	unsigned long lastDisplayUpdateTime = 0;
	byte characterDisplayed = 0;

	char buff[8];

	// Long string
	const char *longString = nullptr;
	byte longStringLength = 0;

	void updateOneDisplay(unsigned long distance, byte digit);
};

#endif
