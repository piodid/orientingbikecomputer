#include "ButtonsController.h"
#include "Pins.h"

ButtonsController::ButtonsController()
{
	for (unsigned char i = 0; i < 5; i++) {
		lastStates[i] = false;
	}

	pins[0] = PIN_BUTTON_LEFT;
	pins[1] = PIN_BUTTON_MODE_LEFT;
	pins[2] = PIN_BUTTON_RIGHT;
	pins[3] = PIN_BUTTON_MODE_RIGHT;
	pins[4] = PIN_BUTTON_LIGHT;
}

void ButtonsController::CheckButtons()
{
	bool states[5];
	for (unsigned char i=0; i<5; i++) {
		states[i] = digitalRead(pins[i]) == LOW;
	}

	unsigned long currentTime = millis();
	if (currentTime - lastEvenTime > TIME_BETWEEN_BUTTON_EVENTS) 
	{
		bool clicked = false;
		for (unsigned char i = 0; i < 4; i++)  {
			if (states[i] && lastStates[i]) 
			{
				unsigned char keyPressed = 0;
				for (unsigned char j = 0; j < 5; j++) {
					if (states[i]) {
						keyPressed |= 1 << j;
					}
				}

				if (states[4]) {
					ignoreReleaseLightButton = true;
				}

				ButtonClicked(1 << i, keyPressed);
				clicked = true;
				break;
			}
		}

		if (!clicked) {
			if (!states[4] && lastStates[4]) {
				if (ignoreReleaseLightButton) {
					ignoreReleaseLightButton = false;
				} else {
					ButtonClicked(Button::LightButton, 0);
				}
			}
		}
	}

	for (unsigned char i = 0; i < 5; i++) {
		lastStates[i] = states[i];
	}
}


//ButtonsController::~ButtonsController() { }
