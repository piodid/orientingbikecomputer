#ifndef BUTTONS_CONTROLLER_H
#define BUTTONS_CONTROLLER_H

#include <Arduino.h>

#define TIME_BETWEEN_BUTTON_EVENTS 500 // ms

class ButtonsController
{
public:
	ButtonsController();
	//~ButtonsController();

	enum Button : unsigned char
	{
		LeftButton = 1, 
		RightButton = 2,
		LeftModeButton = 4,
		RightModeButton = 8,
		LightButton = 16,
	};

	void CheckButtons();

	// Params: key triggered, key pressed
	void (*ButtonClicked)(unsigned char, unsigned char) = nullptr;

private:
	unsigned long lastEvenTime = 0;
	unsigned char pins[5];
	bool lastStates[5];
	bool ignoreReleaseLightButton = false;
};

#endif
